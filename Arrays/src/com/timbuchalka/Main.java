package com.timbuchalka;

import java.util.Scanner;

public class Main {

    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        int[] table = arrayOne(7);
        showArray(table);
        sortTheArray(table);
        showArray(table);

    }

    // method to create a table of integers
    public static int[] arrayOne(int number) {

        System.out.println("Enter " + number + "integer values.");

        int[] array = new int[number];

        for (int i = 1; i <= array.length; i++) {
            System.out.println("Enter #" + i + " value");
            array[i - 1] = scanner.nextInt();
        }
        return array;
    }


    // method to show the existing array
    public static void showArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println();
    }

    // method that sorts a list of integer in decending order;
    public static void sortTheArray(int[] array) {

        int temp;
        boolean swapped = true;
        int numberOfIterations = 0;

        for (int i = 0; i < array.length; i++) {

                if (swapped) {
                    for (int j = 0; j < array.length - (i + 1); j++) {

                        if (array[j] < array[j + 1]) {
                            temp = array[j];
                            array[j] = array[j + 1];
                            array[j + 1] = temp;
                            swapped = true;
                        } else {
                            swapped = false;
                        }

                    }
                } else {
                    break;
                }

            numberOfIterations++;

        }
        System.out.println("Iteration number " + numberOfIterations);

    }


}
