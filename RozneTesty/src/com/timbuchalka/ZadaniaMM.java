package com.timbuchalka;

import java.util.Scanner;

public class ZadaniaMM {

    //urszula.swiatlowska@akademiakodu.pl

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        String dane = scanner.nextLine();

        String reversed = dane.substring(dane.indexOf(" ") + 1).concat(" ").concat(dane.substring(0, dane.indexOf(" ")));
        System.out.println(reversed);

        String[] toBeReversed = dane.split(" ");
        for (int i = toBeReversed.length - 1; i >= 0; i--) {
            System.out.print(toBeReversed[i] + " ");
        }
        System.out.println();
        System.out.println(dane.startsWith("J"));
        System.out.println(dane.length() - 1);

        System.out.println(dane.contains("miel"));
    }

}
