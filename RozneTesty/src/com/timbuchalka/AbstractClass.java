package com.timbuchalka;

public abstract class AbstractClass {

    String name;
    int i;

    public AbstractClass(String name, int i) {
        this.name = name;
        this.i = i;
    }

    public abstract void defineI();
    public abstract void defineName();

    public void gdzieJestes(String name){
        System.out.println("jestem tutaj " + name);
    }


}
