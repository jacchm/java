package com.timbuchalka;

public class Main {

    public static void main(String[] args) {

        Vehicle pojazd = new Vehicle();
        Vehicle auto = new Car();
        Vehicle rower = new Bike();
        Car samochod = new Car();

        System.out.println(auto instanceof Vehicle);
        System.out.println(samochod instanceof Vehicle);
        System.out.println(pojazd instanceof Car);

        AbstractClass dupa = new InheritAbstractClass("Tom", 5);

        Temporary.zrobMiCos();

    }

}
