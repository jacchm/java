package com.timbuchalka;

public class Car extends Vehicle{

    @Override
    public void startVehicle() {
        System.out.println("WRUUUM car has just started!!!!! GOGOGO!");
    }

    public final void startCar(){
        this.name = "Auto";
        System.out.println("startCar() method invoked " + this.name);
    }

    public final void startCar(int a){
        this.name = "auto";
        System.out.println("StartCar() method invoked " + this.name + " zasuwamy z predkoscia " + a);
    }




//    void jadeVehiclem(String name) {
//        System.out.println("Jade samochodem");
//    }

}
