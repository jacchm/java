package com.timbuchalka;

public class FinalAbstractClass {

    public final String STUDENT_CARD_ID;

    public FinalAbstractClass(String studentCardId) {
        STUDENT_CARD_ID = studentCardId;
    }

    public void getStudentCardId() {
        System.out.println(this.STUDENT_CARD_ID);
    }



}
