package com.timbuchalka;

public class Hamburger {


    private String breadRollType; //white or dark
    private String meat;          //beef, goat, lamb, chicken, turkey, pork
    private double priceOfBurger;

    private String addition1Name;
    private double addition1Price;
    private String addition2Name;
    private double addition2Price;
    private String addition3Name;
    private double addition3Price;
    private String addition4Name;
    private double addition4Price;

    public Hamburger(String breadRollType, String meat, String addition1Name,
                     String addition2Name, String addition3Name, String addition4Name) {

        double priceOfMeat = 0;
        double priceOfRoll = 0;

        switch(meat){
            case "beef":
                this.meat = "beef";
                priceOfMeat = 2.0;
                break;
            case "goat":
                this.meat = "goat";
                priceOfMeat = 2.5;
                break;
            case "lamb":
                this.meat = "lamb";
                priceOfMeat = 3.0;
                break;
            case "chicken":
                this.meat = "chicken";
                priceOfMeat = 1.5;
                break;
            case "turkey":
                this.meat = "turkey";
                priceOfMeat = 2.0;
                break;
            case "pork":
                this.meat = "pork";
                priceOfMeat = 4.5;
                break;
            default: { //beef as default meat
                this.meat = "beef";
                priceOfMeat = 2.0;
                break;
            }
        }


        switch(breadRollType){
            case "white":
                this.breadRollType = "white";
                priceOfRoll = 1.0;
                break;
            case "dark":
                this.breadRollType = "dark";
                priceOfRoll = 1.5;
                break;
            default:
                this.breadRollType = "white";
                priceOfRoll = 1.0;
                break;
        }

        System.out.println("Cost of meat is " + priceOfMeat);
        System.out.println("Cost of roll is " + priceOfRoll);

        this.addition1Name = addition1Name;
        this.addition2Name = addition2Name;
        this.addition3Name = addition3Name;
        this.addition4Name = addition4Name;

    }

    public double getHamburgerPrice(){
        return this.priceOfBurger;
    }

    public String getBreadRollType() {
        return breadRollType;
    }

    public String getMeat() {
        return meat;
    }
}
