package com.timbuchalka;

public class PC {

    private Case theCase;
    private Monitor Monitor;
    private Motherboard motherboard;

    public PC(Case theCase, com.timbuchalka.Monitor monitor, Motherboard motherboard) {
        this.theCase = theCase;
        Monitor = monitor;
        this.motherboard = motherboard;
    }

    public void powerUp(){
        theCase.pressPowerButton();
        drawLogo();
    }
    private void drawLogo(){
        //Fancy graphics
        this.Monitor.drawPixelAt(50,1200, "yellow");
    }

    private Case getTheCase() {
        return theCase;
    }

    private com.timbuchalka.Monitor getMonitor() {
        return Monitor;
    }

    private Motherboard getMotherboard() {
        return motherboard;
    }
}
