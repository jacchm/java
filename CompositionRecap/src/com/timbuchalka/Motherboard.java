package com.timbuchalka;

public class Motherboard {

    private String model;
    private String manufacturer;
    private int numberOfRamSlots;
    private int numberOfSlots;
    private String bios;

    public Motherboard(String model, String manufacturer, int numberOfRamSlots, int numberOfSlots, String bios) {
        this.model = model;
        this.manufacturer = manufacturer;
        this.numberOfRamSlots = numberOfRamSlots;
        this.numberOfSlots = numberOfSlots;
        this.bios = bios;
    }

    public String getModel() {
        return model;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public int getNumberOfRamSlots() {
        return numberOfRamSlots;
    }

    public int getNumberOfSlots() {
        return numberOfSlots;
    }

    public String getBios() {
        return bios;
    }
    public void loadProgram(String programName){
        System.out.println("Program " + programName + " is now loading.");
    }


}
