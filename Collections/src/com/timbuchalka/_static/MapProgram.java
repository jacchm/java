package com.timbuchalka._static;

import java.util.HashMap;

public class MapProgram {

    public static void main(String[] args) {
        java.util.Map<String, String> languages = new HashMap<>();


        if (languages.containsKey("Java")){
            System.out.println("Java is already in the map");
        }else{
            languages.put("Java", "a compiled high level, object-orientated, platform independant language");
            System.out.println("Java added succesfully");
        }


        languages.put("Python", "an intepreted object-orientated, high level programming lanugague with dynamic semantics");
        languages.put("Algol", "an algorithmic language");
        languages.put("BASIC", "beginners all purposes symbolic instruction code");
        System.out.println(languages.put("Lisp", "Therein lies madness"));


        if (languages.containsKey("Java")){
            System.out.println("Java is already in the map");
        }else{
            languages.put("Java", "this course is about Java");
        }

        System.out.println("===========================================================================");

//        languages.remove("Lisp");
        if(languages.remove("Algol", "a family of algorithmic languages")){
            System.out.println("Algol removed");
        }else{
            System.out.println("Algol not removed, key/value pair not found");
        }


        if (languages.replace("Lisp","This will not work",
                "a functional programming language with imperative features")){
            System.out.println("Lisp replaced");
        }else{
            System.out.println("Lisp was not replaced");
        }

//        System.out.println(languages.replace("Scala", "this will not be added"));
        for(String key : languages.keySet()){
            System.out.println(key + " : " + languages.get(key));
        }


    }

}
