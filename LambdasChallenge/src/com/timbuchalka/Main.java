package com.timbuchalka;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {

        //Challenge 1
//        Runnable runnable = new Runnable() {
//            @Override
//            public void run() {
//                String myString = "Let's split this up into an array";
//                String[] parts = myString.split(" ");
//                for (String part : parts){
//                    System.out.println(part);
//                }
//            }
//        };
//        runnable.run();

        Runnable runnable1 = () -> {
            String myString = "Let's split this up into an array";
            String[] parts = myString.split(" ");
            for (String part : parts) {
                System.out.println(part);
            }
        };

        runnable1.run();

        System.out.println(everySecondChar("DUPADUPADUPA"));

        //Challenge 2 Tim's sollution
        Function<String, String> lambdaFunction = s -> {
            StringBuilder returnVal = new StringBuilder();
            for (int i = 0; i < s.length(); i++) {
                if (i % 2 == 1) {
                    returnVal.append(s.charAt(i));
                }
            }
            return returnVal.toString();
        };

        // Challenge 3
        System.out.println(lambdaFunction.apply("1234567890"));

        // Challenge 4 and 5
        System.out.println(everySecondCharacter(lambdaFunction,"1234567890"));

        // Challenge 6
        Supplier<String> iLoveJava = () -> "I love JAVA!";
        System.out.println(iLoveJava.get());

        // Challenge 7
        String supplierResult = iLoveJava.get();
        System.out.println(supplierResult);


        System.out.println("=============================");
        //Challenge 9
        List<String> topNames2015 = Arrays.asList(
                "Amelia",
                "Olivia",
                "emily",
                "Isla",
                "Ava",
                "oliver",
                "Jack",
                "Charlie",
                "harry",
                "Jacob"
        );

        // Challenge 10 and 11
        topNames2015
                .stream()
                .map(s -> {
                    if (Character.isLowerCase(s.charAt(0))){
                        String result = Character.toUpperCase(s.charAt(0)) + s.substring(1);
                        return result;
                    }
                    return s;
                })
                .sorted()
                .forEach(System.out::println);

        // Challenge 10 (Tim's sollution)
        List<String> firstUpperCaseList = new ArrayList<>();
        topNames2015.forEach(name ->
                firstUpperCaseList.add(name.substring(0,1).toUpperCase() + name.substring(1)));
//        firstUpperCaseList.sort((s1,s2) -> s1.compareTo(s2));
//        firstUpperCaseList.forEach(s -> System.out.println(s));
        firstUpperCaseList.sort(String::compareTo);
        firstUpperCaseList.forEach(System.out::println);

        // Challenge 11 Tim's sollution
        topNames2015
                .stream()
                .map(s -> s.substring(0,1).toUpperCase() + s.substring(1))
                .sorted(String::compareTo)
                .forEach(System.out::println);

        // Challenge 12
        System.out.println(topNames2015
                .stream()
                .map(s -> s.substring(0,1).toUpperCase() + s.substring(1))
                .filter(s -> s.startsWith("A"))
                .count());


            // check if starts with lower case
//                .filter(s -> Character.isLowerCase(s.charAt(0)))

//        someBingoNumbers
//                .stream()
//                .map(String::toUpperCase)
//                .filter(s -> s.startsWith("G"))
//                .sorted()
//                .forEach(System.out::println);

        // Challenge 13
        System.out.println("++CHALLENGE 13++");
        topNames2015
                .stream()
                .map(s -> s.substring(0,1).toUpperCase() + s.substring(1))
                .peek(System.out::println)
                .sorted(String::compareTo);

        // Challenge 14
        System.out.println("++CHALLENGE 14++");
        topNames2015
                .stream()
                .map(s -> s.substring(0,1).toUpperCase() + s.substring(1))
                .peek(System.out::println)
                .filter(s -> s.startsWith("A"))
                .count();

        System.out.println("++CHALLENGE 14 tim++");
        topNames2015
                .stream()
                .map(s -> s.substring(0,1).toUpperCase() + s.substring(1))
                .peek(System.out::println)
                .sorted(String::compareTo)
                .collect(LinkedList::new, LinkedList::add, LinkedList::addAll);

    }

    //Challenge 2
    public static String everySecondChar(String source) {

        Supplier<String> everySecond = () -> {
            String s = "";
            for (int i = 0; i < source.length(); i++) {
                if (i % 2 == 1) {
                    s += source.charAt(i);
                }
            }
            return s;
        };
        return everySecond.get();
    }

    // Challenge 4
    public static String everySecondCharacter(Function<String, String> function, String s){
        return function.apply(s);
    }




}