package com.timbuchalka;

public class Main {

    public static void main(String[] args) {

        Printer printer = new Printer(true);
        System.out.println(printer.getNumberOfPagesPrinted());

        printer.printPages(50);
        System.out.println(printer.getNumberOfPagesPrinted());
        System.out.println(printer.getTonerLevel());



    }
}
