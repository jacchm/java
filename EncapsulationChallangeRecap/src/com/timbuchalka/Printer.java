package com.timbuchalka;

public class Printer {

    private double tonerLevel;
    private int numberOfPagesPrinted;
    private boolean isDuplex;

    public Printer(boolean isDuplex) {
        this.tonerLevel = 100;
        this.numberOfPagesPrinted = 0;
        this.isDuplex = isDuplex;
    }

    public double getTonerLevel() {
        return tonerLevel;
    }

    public int getNumberOfPagesPrinted() {
        return numberOfPagesPrinted;
    }

    public void isDuplex() {
        if(isDuplex){
            System.out.println("Printer is DUPLEX printer");
        }else{
            System.out.println("Printer is not a DUPLEX printer");
        }
    }

    public void fillUpToner(double addTonerLevel){
        if(addTonerLevel<0){
            System.out.println("Error");
        }else if(addTonerLevel+this.tonerLevel>100){
            System.out.println("Toner level filled up to max. You still have " + );
        }else{
            System.out.println("Toner level filled up to " + this.tonerLevel + "level");
        }
    }

    public void printPages(int numberOfPages){
        if(numberOfPages<0){
            System.out.println("Error");
        }else{
            this.numberOfPagesPrinted = this.numberOfPagesPrinted + numberOfPages;
            this.tonerLevel = this.tonerLevel - 0.01*numberOfPages;
            System.out.println("Printing has just started.");
        }
    }




}
