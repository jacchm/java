package com.timbuchalka;

public class Car extends Vehicle{

    private String brand;
    private int radioOnOff;
    private String drive;

    public Car(String vehicleType, int numberOfWheels, int numberOfGears, int currentSpeed, int maxSpeed, String brand, int radioOnOff, String drive) {
        super(vehicleType, numberOfWheels, numberOfGears, currentSpeed, maxSpeed);
        this.brand = brand;
        this.radioOnOff = radioOnOff;
        this.drive = drive;
    }

    public void turnRadio(int turnRadioOnOff){
        if (turnRadioOnOff==1){
            this.radioOnOff = 1;
            System.out.println("Turned the radio on.");
        }else if(turnRadioOnOff==0){
            this.radioOnOff = 0;
            System.out.println("Turned off the radio");
        }else{
            System.out.println("Wrong input");
        }
    }

    public String getDrive() {
        return drive;
    }

    public String getBrand() {
        return brand;
    }
}
