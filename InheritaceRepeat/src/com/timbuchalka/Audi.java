package com.timbuchalka;

public class Audi extends Car{

    private String model;
    private int numberOfAirbags;

    public Audi(String vehicleType, int numberOfWheels, int numberOfGears, int currentSpeed, int maxSpeed, String brand,
                int radioOnOff, String drive, String model, int numberOfAirbags) {
        super(vehicleType, numberOfWheels, numberOfGears, currentSpeed, maxSpeed, brand, radioOnOff, drive);
        this.model = model;
        this.numberOfAirbags = numberOfAirbags;
    }




}
