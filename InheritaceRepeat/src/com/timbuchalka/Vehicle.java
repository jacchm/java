package com.timbuchalka;

public class Vehicle {

    private String vehicleType;
    private int numberOfWheels;
    private int numberOfGears;
    private int currentSpeed;
    private int currentGear;
    private int maxSpeed;

    public Vehicle(String vehicleType, int numberOfWheels, int numberOfGears, int currentSpeed, int maxSpeed) {
        this.vehicleType = vehicleType;
        this.currentSpeed = 0;
        if (numberOfWheels < 0) {
            this.numberOfWheels = 0;
        } else {
            this.numberOfWheels = numberOfWheels;
        }
        if (numberOfGears < 0) {
            this.numberOfGears = 0;
        } else {
            this.numberOfGears = numberOfGears;
        }
        if (maxSpeed < 0) {
            this.maxSpeed = 0;
        } else {
            this.maxSpeed = maxSpeed;
        }

    }

    public void changeGears(int gear) {
        if (Math.abs(gear - currentGear) == 1 && currentGear > 0) {
            System.out.println("Current gear changed. Now is " + gear);
        } else {
            System.out.println("Unable to change");
        }
    }

    public void accelerate(int speed){
        if(speed>0) System.out.println("Acceleration");
        if(speed<0) System.out.println("Deceleration. Breaks has been used.");
        if(speed==0) System.out.println("Keep up current speed");
    }

    public void steering(String turn){
        if(turn=="right"){
            System.out.println("You turned right");
        }else if(turn=="left"){
            System.out.println("You turned left");
        }else{
            System.out.println("WTF are you doing?!");
        }
    }


}
