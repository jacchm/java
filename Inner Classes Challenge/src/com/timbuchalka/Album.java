package com.timbuchalka;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Album {

    private String albumName;
    private String artist;
    private SongList albumOfSongs;

    public Album(String albumName, String artist) {
        this.albumName = albumName;
        this.artist = artist;
        this.albumOfSongs = new SongList();
    }

    public String getAlbumName() {
        return albumName;
    }

    public SongList getAlbumOfSongs() {
        return albumOfSongs;
    }


    public boolean addNewSong(String title, double duration) {
        return this.albumOfSongs.add(new Song(title,duration));
    }


    public boolean addToPlayList(int trackNumber, LinkedList<Song> playlist) {
        if (this.albumOfSongs.findSongInList(trackNumber) != null) {
            playlist.add(this.albumOfSongs.findSongInList(trackNumber));
            return true;
        }
        System.out.println("This album does not have a track " + trackNumber);
        return false;
    }

    public boolean addToPlayList(String title, LinkedList<Song> playList) {
        Song checkedSong = this.albumOfSongs.findSongInList(title);
        if (checkedSong != null) {
            playList.add(checkedSong);
            return true;
        }
        System.out.println("The song " + title + " is not in this album");
        return false;
    }


    private class SongList {

        private List<Song> listOfSongs;

        public SongList() {
            this.listOfSongs = new ArrayList<>();
        }

        public boolean add(Song song) {
            if (listOfSongs.contains(song)) {
                return false;
            }
            this.listOfSongs.add(song);
            return true;
        }

        public Song findSongInList(String title) {
            for (Song checkedSong : this.listOfSongs) {
                if (checkedSong.getTitle().equals(title)) {
                    return checkedSong;
                }
            }
            return null;
        }

        public Song findSongInList(int trackNumber) {
            int index = trackNumber - 1;
            if ((index >= 0) && (index <= this.listOfSongs.size())) {
                return listOfSongs.get(index);
            }
            return null;
        }

    }


}
