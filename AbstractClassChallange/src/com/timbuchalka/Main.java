package com.timbuchalka;

public class Main {

    public static void main(String[] args) {

        MyLinkedList list = new MyLinkedList(null);
        list.traverse(list.getRoot());

        String stringData = "1 2 3";

        String[] data = stringData.split(" ");
        for (String s : data) {
            list.addItem(new Node(s));
        }


        list.traverse(list.getRoot());
        list.removeItem(new Node("4"));
        list.traverse(list.getRoot());


    }
}
