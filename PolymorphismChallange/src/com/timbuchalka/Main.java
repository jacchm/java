package com.timbuchalka;

class Car{
    private int numberOfCylinders;
    private String name;
    private boolean engine;
    private int numberOfWheels;

    public Car(int numberOfCylinders, String name) {
        this.numberOfCylinders = numberOfCylinders;
        this.name = name;
        this.engine = true;
        this.numberOfWheels = 4;
    }

    public int getNumberOfCylinders() {
        return numberOfCylinders;
    }

    public String getName() {
        return name;
    }

    public boolean isEngine() {
        return engine;
    }

    public int getNumberOfWheels() {
        return numberOfWheels;
    }

    public void startEngine(){
        System.out.println("WRUM");
    }

    public void accelerate(int acceleration){
        System.out.println("Car has speed up to: "+ acceleration + " kmph");
    }

    public void useBreaks(){
        System.out.println("Car has started to break");
    }

    public void stopTheCar(){
        System.out.println("Car has stopped");
    }

    public String getNameClass(){
        return getClass().getSimpleName();
    }

}


class Mustang extends Car{
    public Mustang(int numberOfCylinders, String name, boolean engine) {
        super(numberOfCylinders, name);
    }

    @Override
    public void startEngine() {
        System.out.println("WRUM MUSTANG IHAAAA");
    }

    @Override
    public void accelerate(int acceleration) {
        System.out.println("Mustang has increased his speed to: "+ acceleration + " kmph");
    }

    @Override
    public void useBreaks() {
        System.out.println("IHAAA Mustang has decided to break suddenly");
    }

    @Override
    public void stopTheCar() {
        System.out.println("Mustang has stopped IHAAA");
    }
}

class AmgMercedes extends Car{
    public AmgMercedes(int numberOfCylinders, String name, boolean engine) {
        super(numberOfCylinders, name);
    }

    @Override
    public void startEngine() {
        System.out.println("AmgMercedes WRRRRMMMM");
    }

    @Override
    public void useBreaks() {
        System.out.println("AmgMercedes has started to break");
    }

    @Override
    public void stopTheCar() {
        System.out.println("AmgMercedes has stopped");
    }
}

class CheviImpala extends Car{
    public CheviImpala(int numberOfCylinders, String name, boolean engine) {
        super(numberOfCylinders, name);
    }

    @Override
    public void startEngine() {
        System.out.println("CheviImpala WRRRRAAAAAMMMM");
    }

    @Override
    public void accelerate(int acceleration) {
        System.out.println("CheviImpala has increased his speed to: "+ acceleration + " kmph");
    }

    @Override
    public void useBreaks() {
        System.out.println("CheviImpala has started to break");
    }

}





public class Main {

    public static void main(String[] args) {

        int randomNumber = (int) (Math.random() *3 +1);

        switch (randomNumber){
            case 1:
        }

    }
}
