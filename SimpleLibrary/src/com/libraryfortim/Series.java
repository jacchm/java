package com.libraryfortim;

public class Series {

    public static int nSum(int n) {
        int sum = 0;
        for (int i = 0; i <= n; i++) {
            sum += i;
        }
        return sum;
    }

    public static int fibonacci(int n) {
        if (n == 0) {
            return 0;
        } else if (n == 1) {
            return 1;
        } else {
            int a = 0;
            int b = 1;
            int c = 0;
            for (int i = 0; i <= n - 2; i++) {
                c = a + b;
                a = b;
                b = c;

            }
            return c;
        }
    }

    public static int factorial(int n) {
        if (n == 0) {
            return 1;
        } else {
            int factorial = 1;
            for (int i = n; i > 1; i--) {
                factorial = factorial * i;
            }
            return factorial;
        }

    }

}
