package com.timbuchalka;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Main {


    private static Scanner scanner = new Scanner(System.in);
    private static Random random = new Random();

 /*   Reguły gry są proste:

    Martwa komórka, która ma dokładnie 3 żywych sąsiadów, staje się żywa w następnej jednostce czasu (rodzi się)
    Żywa komórka z 2 albo 3 żywymi sąsiadami pozostaje nadal żywa; przy innej liczbie sąsiadów umiera (z „samotności” albo „zatłoczenia”).*/


    public static void main(String[] args) {

        System.out.println("Podaj wielkość planszy: ");
        int size = scanner.nextInt();

        int[][] gameTable = new int[size][size];

        System.out.println("Podaj procent żywych komórek na start: ");
        double liveCellRatio = scanner.nextInt();
        liveCellRatio = liveCellRatio / 100;

        int numberOfLiveCells = (int) (size * size * liveCellRatio);
//        System.out.println("Number of live cells is: " + numberOfLiveCells);


/*        // draw of the live cells
        int countOfLiveCells = 0;
        while (countOfLiveCells != numberOfLiveCells) {
            countOfLiveCells = 0;

            for (int i = 0; i < gameTable.length; i++) {
                for (int j = 0; j < gameTable.length; j++) {
                    if (gameTable[i][j] == 0) {
                        gameTable[i][j] = random.nextInt(2);
                    }
                    if (gameTable[i][j] == 1) {
                        countOfLiveCells++;
                    }
                    if (countOfLiveCells == numberOfLiveCells) {
                        break;
                    }
                }
                if (countOfLiveCells == numberOfLiveCells) {
                    break;
                }
            }

//            System.out.println(countOfLiveCells);

        }*/

        gameTable[1][1] = 1;
        gameTable[2][2] = 1;
        gameTable[2][3] = 1;
//        gameTable[1][3] = 1;
//        gameTable[0][3] = 1;


        // printing
        for (int i = 0; i < gameTable.length; i++) {
            for (int j = 0; j < gameTable.length; j++) {
                System.out.print(gameTable[i][j] + " ");
            }
            System.out.println();
        }

        // game


        int[][] previousGameTable =  new int[size][size];

 /*       // printing
        System.out.println("Copy of table");
        for (int i = 0; i < gameTable.length; i++) {
            for (int j = 0; j < gameTable.length; j++) {
                System.out.print(gameTable[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();*/

        int counter = 0;
        int loop = 0;

        while(true) {
            loop++;

            for (int i = 0; i < gameTable.length; i++) {
                previousGameTable[i] = Arrays.copyOf(gameTable[i], previousGameTable[i].length);
            }


            for (int i = 0; i < previousGameTable.length; i++) {
                counter = 0;
                for (int j = 0; j < previousGameTable.length; j++) {
                    counter = 0;
//                    System.out.println("i = " + i + " j = " + j + "counter = " + counter);
                    // top left corner
                    if (i == 0 && j == 0) {
//                        System.out.println("top left corner cell used");
                        for (int m = i; m <= i + 1; m++) {
                            for (int n = j; n <= j + 1; n++) {
                                if (m == i && n == j) {
                                    continue;
                                }
                                if (previousGameTable[m][n] == 1) {
                                    counter++;
                                }
                            }
                        }
                    }

                    // top right corner
                    else if (i == 0 && j == previousGameTable.length - 1) {
//                        System.out.println("top right corner cell used");
                        for (int m = i; m <= i + 1; m++) {
                            for (int n = j - 1; n <= j; n++) {
                                if (m == i && n == j) {
                                    continue;
                                }
                                if (previousGameTable[m][n] == 1) {
                                    counter++;
                                }
                            }
                        }
                    }

                    // bottom left corner
                    else if (i == previousGameTable.length - 1 && j == 0) {
//                        System.out.println("bottom left corner cell used");
                        for (int m = i - 1; m <= i; m++) {
                            for (int n = j; n <= j + 1; n++) {
                                if (m == i && n == j) {
                                    continue;
                                }
                                if (previousGameTable[m][n] == 1) {
                                    counter++;
                                }
                            }
                        }
                    }

                    // bottom right corner
                    else if (i == previousGameTable.length - 1 && j == previousGameTable.length - 1) {
//                        System.out.println("bottom right corner cell used");
                        for (int m = i - 1; m <= i; m++) {
                            for (int n = j - 1; n <= j; n++) {
                                if (m == i && n == j) {
                                    continue;
                                }
                                if (previousGameTable[m][n] == 1) {
                                    counter++;
                                }
                            }
                        }
                    }

                    // top side
                    else if (i == 0 && j != 0 && j != previousGameTable.length - 1) {
//                        System.out.println("top side cell used");
                        for (int m = i; m <= i + 1; m++) {
                            for (int n = j - 1; n <= j + 1; n++) {
                                if (m == i && n == j) {
                                    continue;
                                }
                                if (previousGameTable[m][n] == 1) {
                                    counter++;
                                }
                            }
                        }
                    }

                    // left side
                    else if (j == 0 && i != 0 && i != previousGameTable.length - 1) {
//                        System.out.println("left side cell used");
                        for (int m = i - 1; m <= i + 1; m++) {
                            for (int n = j; n <= j + 1; n++) {
                                if (m == i && n == j) {
                                    continue;
                                }
                                if (previousGameTable[m][n] == 1) {
                                    counter++;
                                }
                            }
                        }
                    }

                    // right side
                    else if (j == previousGameTable.length - 1 && i != 0 && i != previousGameTable.length - 1) {
//                        System.out.println("ride side cell used");
                        for (int m = i - 1; m <= i + 1; m++) {
                            for (int n = j - 1; n <= j; n++) {
                                if (m == i && n == j) {
                                    continue;
                                }
                                if (previousGameTable[m][n] == 1) {
                                    counter++;
                                }
                            }
                        }
                    }

                    // bottom side
                    else if (i == previousGameTable.length - 1 && j != 0 && j != previousGameTable.length - 1) {
//                        System.out.println("bottom side cell used");
                        for (int m = i - 1; m <= i; m++) {
                            for (int n = j - 1; n <= j + 1; n++) {
                                if (m == i && n == j) {
                                    continue;
                                }
                                if (previousGameTable[m][n] == 1) {
                                    counter++;
                                }
                            }
                        }
                    }

                    // middle cell
                    else {
//                        System.out.println("middle cell used");
                        for (int m = i - 1; m <= i + 1; m++) {
                            for (int n = j - 1; n <= j + 1; n++) {
                                if (m == i && n == j) {
                                    continue;
                                }
                                if (previousGameTable[m][n] == 1) {
                                    counter++;
                                }
                            }
                        }
                    }

        /*Martwa komórka, która ma dokładnie 3 żywych sąsiadów, staje się żywa w następnej jednostce czasu (rodzi się)
        Żywa komórka z 2 albo 3 żywymi sąsiadami pozostaje nadal żywa; przy innej liczbie sąsiadów umiera (z „samotności” albo „zatłoczenia”).*/
//                    System.out.println("#0 Position: " + i + " " + j + " Previous value: " + previousGameTable[i][j] + " Counter: " + counter);
                    // change of cells' vitality
                    if (previousGameTable[i][j] == 0 && counter == 3) {
                        gameTable[i][j] = 1;
//                        System.out.println("#1 Position: " + i + " " + j + " Previous value: " + previousGameTable[i][j] + " Counter: " + counter + " New value: " + gameTable[i][j]);
                    } else if (previousGameTable[i][j] == 0 && counter != 3) {
                        gameTable[i][j] = 0;
//                        System.out.println("#2 Position: " + i + " " + j + " Previous value: " + previousGameTable[i][j] + " Counter: " + counter + " New value: " + gameTable[i][j]);
                    } else if (previousGameTable[i][j] == 1 && counter != 2 && counter != 3) {
                        gameTable[i][j] = 0;
//                        System.out.println("#3 Position: " + i + " " + j + " Previous value: " + previousGameTable[i][j] + " Counter: " + counter + " New value: " + gameTable[i][j]);
                    } else if (previousGameTable[i][j] == 1 && (counter == 2 || counter == 3)) {
                        gameTable[i][j] = 1;
//                        System.out.println("#4 Position: " + i + " " + j + " Previous value: " + previousGameTable[i][j] + " Counter: " + counter + " New value: " + gameTable[i][j]);
                    } else {
//                        System.out.println("ERROR :) Position: " + i + " " + j + " Previous value: " + previousGameTable[i][j] + " Counter: " + counter + " New value: " + gameTable[i][j]);
                    }
//                    System.out.println();

                }
            } // end of live neighbours count


            // printing
            System.out.println();
            for (int i = 0; i < gameTable.length; i++) {
                for (int j = 0; j < gameTable.length; j++) {
                    System.out.print(gameTable[i][j] + " ");
                }
                System.out.println();
            }
            System.out.println();
        }





    }
}
