package com.timbuchalka;

public class BankAccount {

    private String accountNumber;
    private double balance;
    private String customerName;
    private String customerEmailAddress;
    private String phoneNumber;

    public BankAccount() {
        this("123456", 0.00, "Default Name", "Default Address",
                "Default Phone Number");
        System.out.println("Empty constructor called");
    }

    public BankAccount(String number, double balance, String customerName, String customerEmailAddress,
                       String phoneNumber) {
        this.accountNumber = number;
        this.balance = balance;
        this.customerName = customerName;
        this.customerEmailAddress = customerEmailAddress;
        this.phoneNumber = phoneNumber;
    }

    public BankAccount(String customerName, String customerEmailAddress, String phoneNumber) {
        this("99999",100.55,customerName,customerEmailAddress,phoneNumber);
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public void setEmail(String email) {
        this.customerEmailAddress = email;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAccountNumber() {
        return this.accountNumber;
    }

    public double getBalance() {
        return this.balance;
    }

    public String getCustomerName() {
        return this.customerName;
    }

    public String getEmail() {
        return this.customerEmailAddress;
    }

    public String getPhoneNumber() {
        return this.getPhoneNumber();
    }

    public void depositFunds(double depositValue) {
        this.balance = this.balance + depositValue;
        System.out.println("You have " + this.balance + " pounds on your account.");
    }

    public void withdrawFunds(double withdrawValue) {
        if (withdrawValue <= this.balance) {
            this.balance -= withdrawValue;
            System.out.println("Take the money from the machine. You have " + this.balance + " pounds left" +
                    " on your account.");
        } else {
            System.out.println("You do not have enough money on your account to perform this action.");
        }
    }


}
