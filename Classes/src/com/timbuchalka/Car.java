package com.timbuchalka;

public class Car {
    private int doors;
    private int wheels;
    private String model;
    private String colour;
    private String engine;

    public void setModel(String model) {

        String validModel = model.toLowerCase();

        if (validModel.equals("carrera") || validModel.equals("911")) {
            this.model = model;
        } else {
            this.model = "Unknown";
        }

    }

    public String getModel() {
        return this.model;
    }


}
