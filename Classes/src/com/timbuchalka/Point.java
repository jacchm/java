package com.timbuchalka;

public class Point {

    private int x;
    private int y;

    public Point() {
    }

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public double distance(){
        double distance = Math.sqrt(this.x*this.x+this.y*this.y);
        return distance;
    }

    public double distance(int x, int y){
        double distance = (double) Math.sqrt((x-this.x)*(x-this.x)+(y-this.y)*(y-this.y));
        return distance;
    }

    public double distance(Point name){

        double distance = (double) Math.sqrt((name.x-this.x)*(name.x-this.x)+(name.y-this.y)*(name.y-this.y));
        return distance;

    }


}
