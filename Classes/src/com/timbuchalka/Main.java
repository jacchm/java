package com.timbuchalka;

public class Main {

    public static void main(String[] args) {

 /*       Car porsche = new Car();
        Car holden = new Car();
        porsche.setModel("911");
        System.out.println("Model is: " + porsche.getModel());
*/

/*    SimpleCalculator calculator = new SimpleCalculator();
    calculator.setFirstNumber(5.0);
    calculator.setSecondNumber(4);
        System.out.println("add= " + calculator.getAdditionResult());
        System.out.println("subtract= "+ calculator.getSubtractionResult());
        calculator.setFirstNumber(5.25);
        calculator.setSecondNumber(0);
        System.out.println("multiply= " + calculator.getMultiplicationResult());
        System.out.println("divide= " + calculator.getDivisionResult());
        System.out.println(calculator.getFirstNumber());
        System.out.println(calculator.getSecondNumber());*/

/*
        BankAccount accountOne = new BankAccount();//("10208540", 0.00, "Jacek Chmiel",
               // "jacekchmiel03@gmail.com", "786117813" );

        accountOne.depositFunds(500);
        accountOne.withdrawFunds(10500);

        BankAccount timsAccount = new BankAccount("Tim", "Tim@gmail.com",
                "12345");
        System.out.println(timsAccount.getAccountNumber());


        */

/*        Wall wall = new Wall(4,4);
        System.out.println("area= " + wall.getArea());
        System.out.println("width= " + wall.getWidth());
        System.out.println("height= " + wall.getHeight());

        wall.setHeight(-1.5);
        System.out.println("width= " + wall.getWidth());
        System.out.println("height= " + wall.getHeight());
        System.out.println("area= " + wall.getArea());*/

/*        Point first = new Point(6,5);
        Point second = new Point(3,1);
        System.out.println("distance(0,0)= " + first.distance());
        System.out.println("distance(second)= " + first.distance(second));
        System.out.println("distance(2,2)= " + first.distance(2,2));
        Point point = new Point();
        System.out.println("distance()= " + point.distance());*/


/*        Carpet carpet = new Carpet(3.5);
        Floor floor = new Floor (2.75, 4.0);
        Calculator calculator = new Calculator(floor, carpet);
        System.out.println("total= " + calculator.getTotalCost());


        carpet = new Carpet(1.5);
        floor = new Floor (5.4, 4.5);
        calculator = new Calculator(floor, carpet);
        System.out.println("total= " + calculator.getTotalCost());*/


        ComplexNumber one = new ComplexNumber(1.0,1.0);
        ComplexNumber two = new ComplexNumber(2.5, -1.5);
        one.add(1,1);
        System.out.println("one real= " + one.getReal());
        System.out.println("imaginary= " + one.getImaginary());
        one.subtract(two);
        System.out.println("one real= " + one.getReal());
        System.out.println("imaginary= " + one.getImaginary());
        two.subtract(one);
        System.out.println("one real= " + two.getReal());
        System.out.println("imaginary= " + two.getImaginary());

    }
}
