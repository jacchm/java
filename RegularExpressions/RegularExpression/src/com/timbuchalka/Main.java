package com.timbuchalka;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {

    public static void main(String[] args) {

        String string = "I am a String. Yes, I am.";
        System.out.println(string);
        String youString = string.replace("I", "You");
        System.out.println(youString);

        String alphanumeric = "abcDeeeF12Gghhiiiijkl99z";
        System.out.println(alphanumeric.replaceAll(".","Y"));
        System.out.println(alphanumeric);

        System.out.println(alphanumeric.replaceAll("^abcDeee", "YYY"));

        String secondString = "abcDeeeF12GghhabcDeeeiiiijkl99z";
        System.out.println(secondString.replaceAll("^abcDeee?", "YYY"));

        System.out.println(alphanumeric.matches("^hello"));
        System.out.println(alphanumeric.matches("^abcDeee"));
        System.out.println(alphanumeric.matches("^abcDeee[*]"));

        System.out.println("---------------------------");
        System.out.println(alphanumeric.replaceAll("jkl99z$"," THE END"));

        System.out.println(alphanumeric.replaceAll("[aei]","X"));
        System.out.println(alphanumeric.replaceAll("[aei][Fj]","X"));

        System.out.println("Harry".replaceAll("[Hh]arry","HARRY"));

        System.out.println(alphanumeric.replaceAll("[^ej]", "X"));
        System.out.println(alphanumeric.replaceAll("[abcdef345678]", "X"));
        System.out.println(alphanumeric.replaceAll("[a-fA-F3-8]", "X"));
        System.out.println(alphanumeric.replaceAll("(?i)[a-f3-8]", "X"));
        System.out.println(alphanumeric.replaceAll("[0-9]", "X"));
        System.out.println(alphanumeric.replaceAll("[\\d]", "X")); // digit
        System.out.println(alphanumeric.replaceAll("\\D", "X")); // non digit

        String hasWhitespace = "I have blanks and\ta tab, and also a newline\n";
        System.out.println(hasWhitespace);
        System.out.println(hasWhitespace.replaceAll("\\s","")); // white character
        System.out.println(hasWhitespace.replaceAll("\t","")); // tab char
        System.out.println(hasWhitespace.replaceAll("\\S","")); // non white character

        System.out.println(alphanumeric.replaceAll("\\w", "X")); //a-zA-z0-9_
        System.out.println(alphanumeric.replaceAll("\\W", "X")); // not a-zA-z0-9_
        System.out.println(hasWhitespace.replaceAll("\\b", "X"));

        System.out.println("----------------------------------------------------");
        String thridAlphanumeric = "abcDeeeF12Gghhiiiijkl99z";
        System.out.println(thridAlphanumeric.replaceAll("^abcDe{3}", "YYY"));
        System.out.println(thridAlphanumeric.replaceAll("^abcDe+", "YYY"));
        System.out.println(thridAlphanumeric.replaceAll("^abcDe*", "YYY"));
        System.out.println(thridAlphanumeric.replaceAll("^abcDe{2,5}", "YYY"));
        System.out.println(thridAlphanumeric.replaceAll("^abcDe{2,5}", "YYY"));
        System.out.println(thridAlphanumeric.replaceAll("h+i*j","Y"));

        // PATTERNS AND MATCHERS

        System.out.println("----------------------------------------------------");
        System.out.println("----------------------------------------------------");
        StringBuilder htmlText = new StringBuilder("<h1>My Heading</h1>");
        htmlText.append("<h2>Sub-heading</h2>");
        htmlText.append("<p>This is a paragraph about something else</p>");
        htmlText.append("<h2>Summary</h2>");
        htmlText.append("<p>Here is the summary.</p>");

        String h2Pattern = "(<h2>)";
        Pattern pattern = Pattern.compile(h2Pattern);
        Matcher matcher = pattern.matcher(htmlText);
        System.out.println(matcher.matches());

        matcher.reset();
        int count = 0;
        while(matcher.find()){
            count++;
            System.out.println("Occurence "+ count + ": " + matcher.start() + " to " + matcher.end());
        }

        //* and + are greedy (longest matchings) // *? +? are lazy (shortest matchings)
        String h2GroupPattern = "(<h2>.*?</h2>)";
        Pattern groupPattern = Pattern.compile(h2GroupPattern);
        Matcher groupMatcher = groupPattern.matcher(htmlText);
        System.out.println(groupMatcher.matches());
        groupMatcher.reset();

        while(groupMatcher.find()){
            System.out.println("Occurence: " + groupMatcher.group(1));
        }

        String h2TextGroups = "(<h2>)(.+?)(</h2>)";
        Pattern h2TextPattern = Pattern.compile(h2TextGroups);
        Matcher h2TextMatcher = h2TextPattern.matcher(htmlText);

        while(h2TextMatcher.find()){
            System.out.println("Occurence "+ h2TextMatcher.group(2));
        }

        // abc a and b and c
        // [[Hh]arry]
        System.out.println("harry".replaceAll("[H|h]arry", "Larry"));
        System.out.println("Harry".replaceAll("[H|h]arry", "Larry"));

        // [^abc]
        String tvTest = "tstvtkt";
//        String tNotVRegExp = "t[^v]";
        String tNotVRegExp = "t(?!v)";
        Pattern tNotVPattern = Pattern.compile(tNotVRegExp);
        Matcher tNotVMatcher = tNotVPattern.matcher(tvTest);

        count = 0;
        while(tNotVMatcher.find()){
            count++;
            System.out.println("Occurrence " + count + " : " + tNotVMatcher.start() + " to " + tNotVMatcher.end());
        }

        //t(?=v) - positive look ahead - t followed by v but doesn't include v in a match
        String phone1 = "1234567890";
        String phone2 = "(123) 456-7890";
        String phone3 = "123 456-7890";
        String phone4 = "(123)456-7890";

        System.out.println("phone1 = " + phone1.matches("^([\\(]{1}[0-9]{3}[\\)]{1}[ ]{1}[0-9]{3}[\\-]{1}[0-9]{4})$"));
        System.out.println("phone2 = " + phone2.matches("^([\\(]{1}[0-9]{3}[\\)]{1}[ ]{1}[0-9]{3}[\\-]{1}[0-9]{4})$"));
        System.out.println("phone3 = " + phone3.matches("^([\\(]{1}[0-9]{3}[\\)]{1}[ ]{1}[0-9]{3}[\\-]{1}[0-9]{4})$"));
        System.out.println("phone4 = " + phone4.matches("^([\\(]{1}[0-9]{3}[\\)]{1}[ ]{1}[0-9]{3}[\\-]{1}[0-9]{4})$"));

        // visa cards regexp
        String visa1 = "4444444444444";
        String visa2 = "5444444444444";
        String visa3 = "4444444444444444";
        String visa4 = "4444";

        System.out.println("visa1 " + visa1.matches("^4[0-9]{12}([0-9]{3})?$"));
        System.out.println("visa2 " + visa2.matches("^4[0-9]{12}([0-9]{3})?$"));
        System.out.println("visa3 " + visa3.matches("^4[0-9]{12}([0-9]{3})?$"));
        System.out.println("visa4 " + visa4.matches("^4[0-9]{12}([0-9]{3})?$"));


    }
}
