package com.timbuchalka;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexpChallenges {

    public static void main(String[] args) {

        System.out.println("-- Challenge 1 --");
        String challenge1 = "I want a bike.";
        System.out.println(challenge1.matches("I want a bike."));

        System.out.println("-- Challenge 2 --");
        String challenge2 = "I want a ball.";
        String regexp2 = "I want a b(ike|all).";
        System.out.println(challenge1.matches(regexp2));
        System.out.println(challenge2.matches(regexp2));

        String regexp2tim = "I want a \\w+.";
        System.out.println(challenge1.matches(regexp2tim));
        System.out.println(challenge2.matches(regexp2tim));

        System.out.println("-- Challenge 3 --");
        Pattern pattern3 = Pattern.compile("I want a \\w+.");
        Matcher matcher3_1 = pattern3.matcher(challenge1);
        System.out.println(matcher3_1.matches());
        Matcher matcher3_2 = pattern3.matcher(challenge2);
        System.out.println(matcher3_2.matches());

        System.out.println("-- Challenge 4 --");
        String challenge4 = "Replace all blanks with underscores";
        String challenge4completed = challenge4.replaceAll(" ", "_");
        System.out.println(challenge4completed);
        challenge4completed = challenge4.replaceAll("\\s", "_");
        System.out.println(challenge4completed);

        System.out.println("-- Challenge 5 --");
        String challenge5 = "aaabccccccccdddefffg";
        String regexp5 = "^([a]{3}bc{8}d{3}e[f]{3}g)$";
        System.out.println(challenge5.matches(regexp5));

        String regexp5tim = "^([a-g]+)";
        System.out.println(challenge5.matches(regexp5tim));

        System.out.println("-- Challenge 6 --");
        String regexp6 = "^([a]{3}bc{8}d{3}e[f]{3}g)$";
        System.out.println(challenge5.matches(regexp6));

        System.out.println("-- Challenge 7 --");
        String challenge7 = "abcd.1235";
        String regexp7 = "^[A-Za-z]+\\.[0-9]+$"; // \\. is a dot character; [0-9] or \\d
        System.out.println(challenge7.matches(regexp7));

        System.out.println("-- Challenge 8 --");
        String regexp8 = "[A-Za-z]+\\.([0-9]+)+?"; // +? lazy operator
//        System.out.println(challenge7.matches(regexp8));
        String challenge8 = "abcd.1235uvwz.7tzik.999";
        Pattern pattern8 = Pattern.compile(regexp8);
        Matcher matcher8 = pattern8.matcher(challenge8);

        while(matcher8.find()){
            System.out.println("Occurence: " + matcher8.group(1));
        }

        System.out.println("-- Challenge 9 --");
        String regexp9 = "[A-Za-z]+\\.([0-9]+\\s+)+?"; // +? lazy operator
        String challenge9 = "abcd.1235\tuvwz.7\ttzik.999\n";
        Pattern pattern9 = Pattern.compile(regexp9);
        Matcher matcher9 = pattern9.matcher(challenge9);

        System.out.println(challenge9.matches(regexp9));
        while(matcher9.find()){
            System.out.println("Occurence: " + matcher9.group(1));
        }

        System.out.println("-- Challenge 10 --");
        String challenge10 = "abcd.135\tuvqz.7\ttzik.999\n";
        String regexp10 = "[A-Za-z]+\\.(\\d+)\\s"; // +? lazy operator
        Pattern pattern10 = Pattern.compile(regexp10);
        Matcher matcher10 = pattern10.matcher(challenge10);

        while(matcher10.find()){
            System.out.println("Occurence: " + matcher10.group(1) + " start is: " + matcher10.start(1) + " end is: " +
                    + (matcher10.end(1)-1));
        }


        System.out.println("\n-- Challenge 11 --");
        String challenge11 = "{0, 2}, {0, 5}, {1, 3}, {2, 4}";

        String regexp11 = "\\{(.+?)\\}";
        Pattern pattern11 = Pattern.compile(regexp11);
        Matcher matcher11 = pattern11.matcher(challenge11);

        while(matcher11.find()){
            System.out.println("Point: " + matcher11.group(1));
        }

        System.out.println("\n-- Challenge 11a --");
        String challenge11a = "{0, 2}, {0, 5}, {1, 3}, {2, 4} {x, y}";
        String regexp11a = "\\{(\\w+, \\w+)\\}";
        Pattern pattern11a = Pattern.compile(regexp11a);
        Matcher matcher11a = pattern11a.matcher(challenge11a);

        while(matcher11a.find()){
            System.out.println("Point: " + matcher11a.group(1));
        }

        System.out.println("\n-- Challenge 12 --");
        String challenge12 = "11111";
        String regexp12 = "^\\d{5}$";
        Pattern pattern12 = Pattern.compile(regexp12);
        Matcher matcher12 = pattern12.matcher(challenge12);
        System.out.println(matcher12.matches());


        System.out.println("\n-- Challenge 13--");
        String challenge13 = "11111-1111";
        String regexp13 = "^\\d{5}-\\d{4}$";
        Pattern pattern13 = Pattern.compile(regexp13);
        Matcher matcher13 = pattern13.matcher(challenge13);
        System.out.println(matcher13.matches());

        System.out.println("\n-- Challenge 14--");
        String challenge14 = "55555-1111";
        String regexp14 = "^(\\d{5})(-\\d{4})?$";
        Pattern pattern14 = Pattern.compile(regexp14);
        Matcher matcher14 = pattern14.matcher(challenge14);
        System.out.println(matcher14.matches());

































    }
}
