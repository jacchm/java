package com.timbuchalka;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<Integer> items = new ArrayList<>();
        items.add(1);
        items.add(2);
        items.add(3);
//        items.add("tim");
        items.add(5);
        items.add(6);

        printDoubled(items);

    }

    private static void printDoubled(List<Integer> n) {
        for (Object i : n) {
            System.out.println((Integer) i * 2);
        }
    }















}
