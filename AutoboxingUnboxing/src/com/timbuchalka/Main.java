package com.timbuchalka;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {

/*        String[] strArray = new String[10];
        int[] intArray = new int[10];

        ArrayList<String> stringArrayList = new ArrayList<String>();
        stringArrayList.add("Tim");

        Integer integer = new Integer(54);
        Double doubleValue = new Double(12.25);*/

        ArrayList<Integer> intArrayList = new ArrayList<Integer>();
        for(int i = 0; i<10; i++){
            intArrayList.add(Integer.valueOf(i));
        }

        for (int i = 0; i<10; i++){
            System.out.println(i + " --> " + intArrayList.get(i).intValue());
        }

/*
        Integer myInt = 56; // Integer.valueOf(56);
        Double myDouble = 56.25;

        int myIntTwo = myInt; // myInt.intValue(56)
        int myIntThree = myInt.intValue();
*/
//
//        System.out.println("myIntTwo = " + myIntTwo +"\nmyIntTwo = " + myIntThree);

        ArrayList<Double> myDoubleValues = new ArrayList<Double>();
        for (double dbl=0.0; dbl<=10.0; dbl+=0.5){
            myDoubleValues.add(dbl);
        }

        for(int i = 0; i<myDoubleValues.size(); i++){
            double value = myDoubleValues.get(i);
            System.out.println(i + " --> " + value);
        }




/*

        ArrayList<Double> doubleArrayList = new ArrayList<Double>();
        for(int i = 0; i<doubleArrayList.size(); i++){
            doubleArrayList.add(Double.valueOf(i));
        }
*/






    }
}
