package com.timbuchalka;

public class Point {

    private int x;
    private int y;

    public Point() {
    }

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public double distance(){

        double dist;
        dist = Math.sqrt((this.x*this.x)+(this.y*this.y));

        return dist;
    }

    public double distance(int x, int y){
        double dist;
        dist = Math.sqrt((this.x-x)*(this.x-x)+(this.y-y)*(this.y-y));
        return dist;
    }

    public double distance(Point point){
        double dist;
        dist = Math.sqrt((this.x-point.x)*(this.x-point.x)+(this.y-point.y)*(this.y-point.y));
        return dist;
    }

}
