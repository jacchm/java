package com.timbuchalka;

public class BankAccount {

    private String accountNumber;
    private double balance;
    private String customerName;
    private String customerEmail;
    private String phoneNumber;

    public BankAccount(String customerName, String customerEmail, String phoneNumber) {
        this.customerName = customerName;
        this.customerEmail = customerEmail;
        this.phoneNumber = phoneNumber;

    }

    public BankAccount(){
        this("56789", 2.50, "Default name",
                "default@gmail.com", "000000000");
    }
    public BankAccount(String accountNumber, double balance,String customerName, String customerEmail, String phoneNumber){
        this.accountNumber = accountNumber;
        this.balance = balance;
        this.customerName = customerName;
        this.customerEmail = customerEmail;
        this.phoneNumber = phoneNumber;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void depositFunds(double depositAmount) {
        if (depositAmount > 0) {
            this.balance = this.balance + depositAmount;
            System.out.println("Your account balance is: " + this.balance);
        } else {
            System.out.println("Invalid amount");
        }
    }

    public void withdrawFunds(double withdrawAmount) {
        if (withdrawAmount <= 0 || withdrawAmount > balance) {
            System.out.println("Invalid operation. Your current balance is " + balance);
        } else {
            this.balance = this.balance - withdrawAmount;
            System.out.println("You have withdrawn " + withdrawAmount + ". Your balance is " + balance
                    + " now");
        }
    }

}



