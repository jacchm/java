package com.timbuchalka;

public class SomeClass {

    private static int classCounter = 0;
    public final int instanceNumber;
    private String name;



    public SomeClass(String name){
        classCounter++;
        instanceNumber = classCounter;
        System.out.println(name + " created, instance is " + instanceNumber);
    }

    public int getInstanceNumber() {
        return instanceNumber;
    }

}
