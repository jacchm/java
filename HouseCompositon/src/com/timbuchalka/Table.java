package com.timbuchalka;

public class Table {

    private int numberOfLegs;
    private int height;
    private int width;
    private int depth;

    public Table(int numberOfLegs, int height, int width, int depth) {
        this.numberOfLegs = numberOfLegs;
        this.height = height;
        this.width = width;
        this.depth = depth;
    }

    public int getNumberOfLegs() {
        return numberOfLegs;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public int getDepth() {
        return depth;
    }

    public void moveTable(){
        System.out.println("Table has been moved");
    }

}
