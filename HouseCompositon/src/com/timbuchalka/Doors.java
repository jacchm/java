package com.timbuchalka;

public class Doors {

    private int height;
    private int width;
    private boolean openToRight;
    private String material;

    public Doors(int height, int width, boolean openToRight, String material) {
        this.height = height;
        this.width = width;
        this.openToRight = openToRight;
        this.material = material;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public boolean isOpenToRight() {
        return openToRight;
    }

    public String getMaterial() {
        return material;
    }




}
