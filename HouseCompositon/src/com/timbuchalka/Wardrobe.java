package com.timbuchalka;

public class Wardrobe {

    private int widht;
    private int height;
    private int depth;
    private boolean isOpened;

    public Wardrobe(int widht, int height, int depth) {
        this.widht = widht;
        this.height = height;
        this.depth = depth;
        this.isOpened = false;
    }

    public int getWidht() {
        return widht;
    }

    public int getHeight() {
        return height;
    }

    public int getDepth() {
        return depth;
    }

    public void open(boolean open){
        this.isOpened = open;
        if(open == true){
            System.out.println("Wardrobe has been opened");
        }else{
            System.out.println("Wardrobe has been closed");
        }
    }


}
