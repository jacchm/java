package com.timbuchalka;

public class Room {

    private int width;
    private int length;
    private int height;
    private Doors doors;
    private Windows windows;
    private Wardrobe wardrobe;
    private Table table;

    public Room(int width, int length, int height, Doors doors, Windows windows, Wardrobe wardrobe, Table table) {
        this.width = width;
        this.length = length;
        this.height = height;
        this.doors = doors;
        this.windows = windows;
        this.wardrobe = wardrobe;
        this.table = table;
    }

    public int getWidth() {
        return width;
    }

    public int getLength() {
        return length;
    }

    public int getHeight() {
        return height;
    }

    private Doors getDoors() {
        return doors;
    }

    public Windows getWindows() {
        return windows;
    }

    public Wardrobe getWardrobe() {
        return wardrobe;
    }

    private Table getTable() {
        return table;
    }

    public void moveTheTable(){
        getTable().moveTable();
    }

}
