package com.timbuchalka;

public class Windows {

    private int width;
    private int height;
    private boolean isOpenToRight;

    public Windows(int width, int height, boolean isOpenToRight) {
        this.width = width;
        this.height = height;
        this.isOpenToRight = isOpenToRight;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public boolean isOpenToRight() {
        return isOpenToRight;
    }



}
