package com.buchalka;

public class TheForStatementChallange {


    public static double calcualateInterest(double amount, double interestRate) {

        return (amount * (interestRate / 100));

    }


    public static boolean isPrime(int n) {

        if (n == 1) {
            return false;
        }
        for (int i = 2; i <= (long) Math.sqrt(n); i++) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;

    }


    public static void main(String[] args) {

//        System.out.println(calcualateInterest(10000, 2));

//        for (int i = 1; i < 6; i++) {
//
//            System.out.println("Loop " + i + " hello!");
//
//        }
//
//        for (int i = 8; i >= 1; i--) {
//
//            System.out.println("10,000 at " + i + "% interest = " + String.format("%.2f", calcualateInterest(10000, i)));
//        }
        int count = 0;

        for (int i = 1; i <= 100; i++) {

            if (isPrime(i)) {
            count ++;
                System.out.println(i + " is the prime number.");
            }
            if (count==10) {
                System.out.println("Existing for loop");
                break;
            }


        }


    }


}


// using the for statement, call the calculateInterest method with
// the amount of 10000 with an interestRate of 2,3,4,5,6,7, and 8
// and print the results to the console window.

// How would you modify the for loop above to do the same thing as
// shown but to start from 8% and work back to 2%

// Create a for statement using any range of numbers
// Determine if the number is a prime number using the isPrime method
// if it is a prime number, print it out AND increment a count of the
// number of prime numbers found
// if that count is 3 exit the for loop
// hint:  Use the break; statement to exit