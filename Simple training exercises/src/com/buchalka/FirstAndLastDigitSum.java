package com.buchalka;

public class FirstAndLastDigitSum {

    public static int sumFirstAndLastDigit(int number) {
        if (number < 0) {
            return -1;
        }

        int sum = number % 10;
        int digit = 0;

        while (number > 0) {
            digit = number % 10;
            number /= 10;

        }
        sum = sum + digit;
        return sum;

    }


    public static void main(String[] args) {


        System.out.println(sumFirstAndLastDigit(23569));

    }
}
