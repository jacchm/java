package com.buchalka;

public class RectangleStar {

    public static void printSquareStar(int number){

        if(number<5){
            System.out.println("Invalid Value");
        }else{
            int rowCount = number;
            int columnCount = number;

            for(int i = 1; i<=rowCount; i++){

                for (int j=1; j<=columnCount; j++){
                    if( i==1 || j==1 || i==j || j==rowCount-i+1 || i==rowCount || j==columnCount){
                        System.out.print("*");
                    }else{
                        System.out.print(" ");
                    }

                }
                System.out.println();
            }
        }

    }





    public static void main(String[] args) {
    printSquareStar(4);
    }
}
