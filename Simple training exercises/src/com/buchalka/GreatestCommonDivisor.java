package com.buchalka;

public class GreatestCommonDivisor {

    public static int getGreatestCommonDivisor(int first, int second){
        if (first<10 || second<10){
            return -1;
        }
        int min = Math.min(first,second);
        int firstRemainder = 0;
        int secondRemainder = 0;
        int maxDivisor = 1;

        for (int i = 2; i<=min; i++){
            firstRemainder = first%i;
            secondRemainder = second%i;

            if(firstRemainder==0 && secondRemainder==0){
                maxDivisor=i;
            }
        }

        return maxDivisor;

    }





    public static void main(String[] args) {

        System.out.println(getGreatestCommonDivisor(81,153));


    }
}
