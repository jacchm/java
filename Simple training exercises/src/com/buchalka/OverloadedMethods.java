public class OverloadedMethods {


    public static double calcFeetAndInchesToCentimeters(double feet, double inch) {
        if (feet < 0 || inch < 0 || inch > 12) {
            System.out.println("Invalid feet or inches number");
            return -1;
        }
        double centimeters = (feet * 12 + inch) * 2.54;
        System.out.println(feet + " feet " + inch + " inches = " + centimeters + " cm");

        return centimeters;
    }

    public static double calcFeetAndInchesToCentimeters(double inch) {
        if (inch < 0){
            System.out.println("Invalid inches number");
            return -1;
        }
        double feet = (int) inch/12;
        double remainingInches = (int) inch%12;
        double centimeters = calcFeetAndInchesToCentimeters(feet,remainingInches);

        return centimeters;

    }


    public static void main(String[] args) {

        calcFeetAndInchesToCentimeters(157);

    }

}
