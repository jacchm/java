package com.buchalka;

public class SharedDigit {

    public static boolean hasSharedDigit(int firstNumber, int secondNumber) {

        if (firstNumber < 10 || firstNumber > 99 || secondNumber < 10 || secondNumber > 99) {
            return false;
        }

        int firstNumberDigit = 0;
        int secondNumberDigit = 0;

        while (firstNumber != 0) {

            System.out.println("First number is " + firstNumber);

            firstNumberDigit = firstNumber % 10;

            System.out.println("FirstNumberDigit is " + firstNumberDigit);


            int tempSecondNumber = secondNumber;

            while (tempSecondNumber != 0) {
                secondNumberDigit = tempSecondNumber % 10;

                System.out.println("SecondNumberDigit is " + secondNumberDigit);

                if (firstNumberDigit == secondNumberDigit) {
                    return true;
                }

                tempSecondNumber /= 10;

                System.out.println("Second Number is " + secondNumber);

            }

            firstNumber = firstNumber / 10;

        }
        return false;
    }


    public static void main(String[] args) {

        System.out.println("Has shared digit: " + hasSharedDigit(12, 13));

    }


}
