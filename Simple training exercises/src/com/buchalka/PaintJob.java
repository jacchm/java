package com.buchalka;

import java.util.Scanner;

public class PaintJob {

    public static int getBucketCount(double width, double height, double areaPerBucket, int extraBuckets) {

        if (width <= 0 || height <= 0 || areaPerBucket<=0 || extraBuckets < 0) {
            return -1;
        }

        double areaOfWall = width * height;
        double bucketsThatBobNeeds = Math.ceil(areaOfWall/areaPerBucket - (double) extraBuckets);

        return (int) bucketsThatBobNeeds;

    }

    public static int getBucketCount(double width, double height, double areaPerBucket) {

        if (width <= 0 || height <= 0 || areaPerBucket<=0) {
            return -1;
        }

        double areaOfWall = width * height;
        double bucketsThatBobNeeds = Math.ceil(areaOfWall / areaPerBucket);

        return (int) bucketsThatBobNeeds;

    }

    public static int getBucketCount(double area, double areaPerBucket) {

        if (area <= 0 || areaPerBucket<=0) {
            return -1;
        }

        double bucketsThatBobNeeds = Math.ceil(area / areaPerBucket);

        return (int) bucketsThatBobNeeds;

    }


    public static void main(String[] args) {

        System.out.println(getBucketCount(2.75,3.25,2.5,1));

        System.out.println(getBucketCount(7.25,4.3,2.35));

        System.out.println(getBucketCount(3.26,0.75));

    }
}
