public class BarkingDog {

    public static boolean shouldWakeUp(boolean barking, int hourOfTheDay) {

        if (hourOfTheDay <= 23 && hourOfTheDay >= 0) {
            if ((hourOfTheDay < 8 && barking == true) || (hourOfTheDay > 22 && barking == true)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }


    public static void main(String[] args) {
        System.out.println(shouldWakeUp(true, 5));
    }

}
