package com.buchalka;

public class WhileDoWhile {

    public static boolean isEvenNumber(int number) {

        if (number % 2 == 0) {
            return true;
        } else {
            return false;
        }


    }


    public static void main(String[] args) {
        int count = 0;
        int number = 4;
        int finishNumber =20;

        while (number<=finishNumber){
            number ++;
            if(!isEvenNumber(number)){
                continue;
            }
            System.out.println("Even number " + number);
            count++;
            if (count==5){
                break;
            }
        }
        System.out.println("Total number of even numbers found is " + count);
    }


}
