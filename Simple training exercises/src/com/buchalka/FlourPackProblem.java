package com.buchalka;

public class FlourPackProblem {

    public static boolean canPack(int bigCount, int smallCount, int goal) {

        if(bigCount<0 || smallCount<0 ||goal<0){
            return false;
        }

        if (bigCount * 5 + smallCount * 1 == goal) {
            return true;
        } else if (bigCount * 5 + smallCount * 1 < goal) {
            return false;
        } else {

            for (int i = 0; i <= bigCount; i++) {
                int bigSum = 5 * i;
                System.out.println("Big count = " + bigSum);

                for (int j = 0; j <= smallCount; j++) {
                    int smallSum = bigSum + 1 * j;
                    System.out.println("Small + big count = " + smallSum);
                    if (smallSum == goal) {
                        return true;
                    }

                }

            }

        }
        return false;

    }


    public static void main(String[] args) {

        System.out.println(canPack(2, 2, 11));

    }
}
