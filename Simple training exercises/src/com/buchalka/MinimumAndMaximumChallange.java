package com.buchalka;

import java.util.Scanner;

public class MinimumAndMaximumChallange {


    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        boolean isNextInt;
        int max = Integer.MIN_VALUE;
        int min = Integer.MAX_VALUE;


        while(true){

            System.out.println("Enter number:");
            isNextInt = scanner.hasNextInt();

            if(isNextInt){

                int number = scanner.nextInt();

                max = Math.max(max, number);
                min = Math.min(min, number);

            }else{
                break;
            }
            scanner.nextLine();

        }

        System.out.println("Maximum entered value is: " + max + ", minimum entered value is: " + min);
        scanner.close();

    }

}
