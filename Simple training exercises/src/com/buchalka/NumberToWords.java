package com.buchalka;

public class NumberToWords {

    public static void numberToWords(int number) {

        if (number < 0) {
            System.out.println("Invalid Value");
        }
        int calcNumber = reverse(number);

        int digit = 0;
        int printCount = 0;

        while (calcNumber != 0) {

            digit = calcNumber % 10;

            switch (digit) {
                case 0:
                    System.out.println("Zero");
                    break;
                case 1:
                    System.out.println("One");
                    break;
                case 2:
                    System.out.println("Two");
                    break;
                case 3:
                    System.out.println("Three");
                    break;
                case 4:
                    System.out.println("Four");
                    break;
                case 5:
                    System.out.println("Five");
                    break;
                case 6:
                    System.out.println("Six");
                    break;
                case 7:
                    System.out.println("Seven");
                    break;
                case 8:
                    System.out.println("Eight");
                    break;
                case 9:
                    System.out.println("Nine");
                    break;
            }
            printCount++;

            calcNumber /= 10;

        }

        for (int i = printCount + 1; i <= getDigitCount(number); i++) {
            System.out.println("ZERO");
        }


    }


    public static int reverse(int number) {


        int reverse = 0;
        int digit = 0;

        while (number != 0) {

            digit = number % 10;
            reverse = reverse * 10 + digit;

            number /= 10;

        }

        return reverse;
    }


    public static int getDigitCount(int number) {

        if(number<0){
            return -1;
        }

        int countOfDigits = 0;

        if (number == 0) {
            countOfDigits = 1;
            return countOfDigits;

        } else {

            while (number != 0) {
                number /= 10;
                countOfDigits++;

            }
        }

        return countOfDigits;

    }


    public static void main(String[] args) {

//        System.out.println(reverse(-1000));
//        System.out.println(getDigitCount(1000));
        numberToWords(1234);


    }


}
