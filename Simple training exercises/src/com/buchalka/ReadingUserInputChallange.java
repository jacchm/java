package com.buchalka;

import java.util.Scanner;

public class ReadingUserInputChallange {

    public static void main(String[] args) {

        boolean hasNextInt = false;
        int countValidNumbers = 1;
        Scanner scanner = new Scanner(System.in);
        int sum = 0;
        int number = 0;

        while(countValidNumbers <= 10){

            System.out.println("Enter number #" + (countValidNumbers) + ":");
            hasNextInt = scanner.hasNextInt();

            if (hasNextInt) {

                number = scanner.nextInt();

                sum = sum + number;
                countValidNumbers++;


            }else{
                System.out.println("Invalid Number");
            }

            scanner.nextLine();

        }

        System.out.println("Sum of the entered values is: " + sum);
        scanner.close();


    }



}
