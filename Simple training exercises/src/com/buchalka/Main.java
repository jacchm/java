package com.buchalka;

public class Main {

    public static void main(String[] args) {

        char switchValue = 'A';

        switch (switchValue) {
            case 'A':
            case 'B':
            case 'C':
            case 'D':
            case 'E':
                System.out.println("It was A, B, C, D or E. Actually it was " + switchValue + ".");
                break;


            default:
                System.out.println("Not found");
            break;
        }


        String month = "January";

        switch (month.toLowerCase()){
            case "january":
                System.out.println("Jan");
            break;
            case "february":
                System.out.println("Feb");
            break;
            default:
                System.out.println("Not sure");
            break;
    }



    }
}
// Create a new switch statement using char instead of int
// create a new char variable
// create a switch statement testing for
// A, B, C, D, or E
// display a message if any of these are found and then break
// Add a default which displays a message saying not found