package com.buchalka;

public class LargestPrimeFactor {

    public static int getLargestPrime(int number) {

        if (number <= 1) {
            return -1;
        }
        int largestPrime = 0;
        boolean isPrime = false;
        int count = 0;

        for (int i = 1; i <= number; i++) {

            isPrime = false;
            count = 0;

            for (int j = 1; j <= i; j++) {
                if (i % j == 0) {
                    count++;
                }
            }

            if (count == 2) {
                isPrime = true;
            }


            if (number % i == 0 && isPrime == true) {
                largestPrime = i;
            }

        }
        return largestPrime;
    }


    public static void main(String[] args) {

        System.out.println("The largest Prime Dividor is: " + getLargestPrime(21));

    }

}
