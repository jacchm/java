package com.buchalka;

public class LastDigitChecker {

    public static boolean hasSameLastDigit(int a, int b, int c) {

        if (a < 10 || b < 10 || c < 10 || a > 1000 || b > 1000 || c > 1000) {
            return false;
        }

        int aRightDigit = a % 10;
        int bRightDigit = b % 10;
        int cRightDigit = c % 10;

        if (aRightDigit == bRightDigit || bRightDigit == cRightDigit || aRightDigit == cRightDigit) {
            return true;
        } else {
            return false;
        }

    }

    public static boolean isValid(int a){

        if (a < 10 || a > 1000 ) {
            return false;
        }
        return true;

    }




    public static void main(String[] args) {
        System.out.println(hasSameLastDigit(9, 32, 42));

    }

}
