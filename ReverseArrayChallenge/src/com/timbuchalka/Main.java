package com.timbuchalka;

import java.sql.Array;
import java.util.Arrays;

public class Main {

    public static int[] reverse(int[] array) {

        int temp;
        int[] reversedArray = new int[array.length];

        for (int i = 0; i <= array.length / 2; i++) {
            temp = array[i];
            reversedArray[i] = array[(array.length - 1) - i];
            reversedArray[(array.length - 1) - i] = temp;
        }

        return reversedArray;

    }


    public static void main(String[] args) {

        int[] array = {0, 2, 4, 6, 8, 10};
        int[] reversedArray = reverse(array);

        System.out.println(Arrays.toString(array));
        System.out.println(Arrays.toString(reversedArray));


    }
}
