package com.timbuchalka;

import java.sql.SQLOutput;
import java.util.*;

public class Main {

    private static Map<Integer, Location> locations = new HashMap<Integer, Location>();

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        Map<String, Integer> tempExit = new HashMap<>();

        locations.put(0, new Location(0, "You are sitting in front of a computer learning Java", tempExit));


        tempExit = new HashMap<>();
        tempExit.put("W", 2);
        tempExit.put("E", 3);
        tempExit.put("S", 4);
        tempExit.put("N", 5);
        locations.put(1, new Location(1, "You are standing at the end of a road before a small brick building", tempExit));

        tempExit = new HashMap<>();
        tempExit.put("N", 5);
        locations.put(2, new Location(2, "You are at the top of a hill", tempExit));

        tempExit = new HashMap<>();
        tempExit.put("W", 1);
        locations.put(3, new Location(3, "You are inside a building, a well house for a small spring", tempExit));

        tempExit = new HashMap<>();
        tempExit.put("N", 1);
        tempExit.put("W", 2);
        locations.put(4, new Location(4, "You are in a valley of plenty", tempExit));

        tempExit = new HashMap<>();
        tempExit.put("S", 1);
        tempExit.put("W", 2);
        locations.put(5, new Location(5, "You are in the forest", tempExit));



        Map<String, String> vocabulary = new HashMap<String, String>();
        vocabulary.put("QUIT", "Q");
        vocabulary.put("NORTH", "N");
        vocabulary.put("SOUTH", "S");
        vocabulary.put("EAST", "E");
        vocabulary.put("WEST", "W");

        int loc = 1;
        while (true) {
            System.out.println(locations.get(loc).getDescription());
            if (loc == 0) {
                break;
            }

            Map<String, Integer> exits = locations.get(loc).getExits();
            System.out.print("Available exits are: ");

            for (String exit : exits.keySet()) {
                System.out.print(exit + ", ");
            }
            System.out.println();

            String direction = scanner.nextLine().toUpperCase();

            if (direction.length()>1) {
                String[] words = direction.split(" ");

                for (String x : words) {
                    if (vocabulary.containsKey(x)) {
                        direction = vocabulary.get(x);
                        break;
                    }
                }
            }

//            PRIMITIVE IDEA
/*            for (int i = 0; i < words.length; i++) {
                if (words[i].equals("NORTH") || words[i].equals("N")) {
                    direction = "N";
                    break;
                } else if (words[i].equals("SOUTH") ||words[i].equals("S")) {
                    direction = "S";
                    break;
                } else if (words[i].equals("WEST") || words[i].equals("W")) {
                    direction = "W";
                    break;
                } else if (words[i].equals("EAST") || words[i].equals("E")) {
                    direction = "E";
                    break;
                } else if (words[i].equals("QUIT") || words[i].equals("Q")){
                    direction = "Q";
                }
            }*/

            if (exits.containsKey(direction)) {
                loc = exits.get(direction);
            } else {
                System.out.println("You can't go in that direction");
            }
            if (!locations.containsKey(loc)) {
                System.out.println("You cannot go in that direction");
            }

        }


    }
}
