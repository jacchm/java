package com.timbuchalka;

import java.util.ArrayList;

public class Bank {

    private String name;
    private ArrayList<Branches> branchesList;

    public Bank(String name) {
        this.name = name;
        this.branchesList = new ArrayList<Branches>();
    }

    public String getName() {
        return name;
    }

    public ArrayList<Branches> getBranchesList() {
        return branchesList;
    }



    /////////////////////////////// ADD BRANCH
    public void addBranch(String branchName) {
        Branches branch = new Branches(branchName);
        if (findBranch(branch.getBranchName()) >= 0) {
            System.out.println("\nBranch with that name has already been created in Bank.");
        } else {
            branchesList.add(branch);
            System.out.println("\nBranch with name: " + name + " in " + this.name + " has been created.");
        }
    }
    ///////////////////////////////


    /////////////////////////////// ADD CUSTOMER IN BRANCH
    public void addCustomerInBranch(String customersName, double customersInitialTransaction, String customersBranchName){
        if (findBranch(customersBranchName)>=0){
            this.branchesList.get(findBranch(customersBranchName)).addCustomer(customersName,customersInitialTransaction);
        }else{
            System.out.println("Branch with that name: " + customersBranchName + " has not been found.");
        }
    }
    ///////////////////////////////


    /////////////////////////////// FIND BRANCH
    private int findBranch(Branches branch) {
        return this.branchesList.indexOf(branch);
    }

    private int findBranch(String branchName) {
        for (int i = 0; i < this.branchesList.size(); i++) {
            Branches branch = this.branchesList.get(i);
            if (branch.getBranchName().equals(branchName)) {
                return i;
            }
        }
        return -1;
    }
    ///////////////////////////////

    /////////////////////////////// PRINT ALL BRANCHES
    public void printListOfBranches() {
        System.out.println("\nList of the branches in Bank: \n");
        for (int i = 0; i < this.branchesList.size(); i++) {
            System.out.println("#" + (i+1) + " branch name: " + this.branchesList.get(i).getBranchName() + ".");
        }
    }
    ///////////////////////////////


    /////////////////////////////// PRINT CUSTOMERS IN PARTICULAR BRANCH
    public void printCustomersFromBranch(String branchName){
        this.branchesList.get(findBranch(branchName)).printListOfCustomers();
    }
    ///////////////////////////////

    /////////////////////////////// PRINT CUSTOMER's TRANSACTIONS IN PARTICULAR BRANCH
    public void printCustomersTransactionsFromBranch(String branchName, String customerName){
        this.branchesList.get(findBranch(branchName)).printCustomerTransactions(customerName);
    }
    ///////////////////////////////

    /////////////////////////////// QUERY BRANCH
    public void queryBranch(String branchName){
        if(findBranch(branchName)>=0){
            System.out.println("Branch with that name exists.");
        }else{
            System.out.println("branch with that name does not exist.");
        }
    }
    ///////////////////////////////

    /////////////////////////////// QUERY CUSTOMER
    public void queryCustomerInBranch(String customerName, String branchName){
        if(findBranch(branchName)>=0){
            this.branchesList.get(findBranch(branchName)).queryCustomer(customerName);
        }else{
            System.out.println("Branch with that name does not exist.");
        }
    }
    ///////////////////////////////



}
