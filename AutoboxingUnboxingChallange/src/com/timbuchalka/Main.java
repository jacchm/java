package com.timbuchalka;

import java.util.Scanner;

public class Main {


    private static Scanner scanner = new Scanner(System.in);
    private static Bank mainBank = new Bank("Main Bank");

    public static void main(String[] args) {

        // You job is to create a simple banking application.
        // There should be a Bank class
        // It should have an arraylist of Branches
        // Each Branch should have an arraylist of Customers
        // The Customer class should have an arraylist of Doubles (transactions)
        // Customer:
        // Name, and the ArrayList of doubles.
        // Branch:
        // Need to be able to add a new customer and initial transaction amount.
        // Also needs to add additional transactions for that customer/branch
        // Bank:
        // Add a new branch
        // Add a customer to that branch with initial transaction
        // Add a transaction for an existing customer for that branch
        // Show a list of customers for a particular branch and optionally a list
        // of their transactions
        // Demonstration autoboxing and unboxing in your code
        // Hint: Transactions
        // Add data validation.
        // e.g. check if exists, or does not exist, etc.
        // Think about where you are adding the code to perform certain actions




        boolean quit = false;
        while (!quit) {

            printActions();
            System.out.println("\nEnter action: (8 to show available actions)");
            int action = scanner.nextInt();
            scanner.nextLine();

            switch (action) {
                case 0:
                    System.out.println("\nShutting the app down...");
                    quit = true;
                    break;
                case 1:
                    addNewBranch();
                    break;
                case 2:
                    printListOfBranches();
                    break;
                case 3:
                    ifBranchExists();
                    break;
                case 4:
                    addNewCustomer();
                    break;
                case 5:
                    printCustomersFromBranch();
                    break;
                case 6:
                    printCustomersTransactionsFromBranch();
                    break;
                case 7:

                    break;
                case 8:
                    printActions();
                    break;
            }


        }


    }


    private static void printActions(){

        System.out.println("\nAvailable actions: \nPress:");
        System.out.println("0 - to shoutdown\n" +
                "1 - to add a new branch\n" +
                "2 - to print a list of branches\n" +
                "3 - query if a branch bank exists\n" +
                "4 - to add a new customer in particular branch\n" +
                "5 - to print a customer list for particular branch\n" +
                "6 - to print a list of customer's transactions\n" +
                "7 - query if a customer in particular branch exists\n" +
                "8 - to print a list of available actions");
        System.out.println("Choose your action: ");

    }


    private static void addNewBranch(){
        System.out.println("Enter the new branch name:");
        String newBranchName = scanner.nextLine();
        mainBank.addBranch(newBranchName);
    }

    private static void printListOfBranches(){
        mainBank.printListOfBranches();
    }

    private static void ifBranchExists(){
        System.out.println("Enter the branch name:");
        String branchName = scanner.nextLine();
        mainBank.queryBranch(branchName);
    }

    private static void addNewCustomer(){
        System.out.println("Enter the customer's branch name:");
        String customersBranchName = scanner.nextLine();
        System.out.println("Enter the customer's name:");
        String customersName = scanner.nextLine();
        System.out.println("Enter the customer's initial transaction:");
        double customersInitialTransaction = scanner.nextDouble();

        mainBank.addCustomerInBranch(customersName, customersInitialTransaction, customersBranchName);
    }

    private static void printCustomersFromBranch(){
        System.out.println("Enter the branch name");
        String branchName = scanner.nextLine();
        mainBank.printCustomersFromBranch(branchName);
    }

    private static void printCustomersTransactionsFromBranch(){
        System.out.println("Enter the branch's name");
        String branchName = scanner.nextLine();
        System.out.println("Enter the customer's name");
        String customerName = scanner.nextLine();
        mainBank.printCustomersTransactionsFromBranch(branchName,customerName);
    }

    private static void queryCustomerFromBranch(){
        System.out.println("Enter the branch's name:");
        String branchName = scanner.nextLine();
        System.out.println("Enter the customer name:");
        String customerName = scanner.nextLine();
        mainBank.queryCustomerInBranch(customerName,branchName);
    }
































}

