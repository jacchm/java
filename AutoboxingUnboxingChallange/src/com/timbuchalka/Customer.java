package com.timbuchalka;

import java.util.ArrayList;

public class Customer {

    private String name;
    private ArrayList<Double> transactions;

    public Customer(String name, double initialTransaction) {
        this.name = name;
        this.transactions = new ArrayList<>();
        this.transactions.add(Double.valueOf(initialTransaction));
    }

    public String getName() {
        return name;
    }

    public ArrayList<Double> getTransactions() {
        return transactions;
    }

    public Customer newCustomer(String name, double initialTransaction) {
        return new Customer(name, initialTransaction);
    }

    public void newTransaction(double transaction) {
        this.transactions.add(Double.valueOf(transaction));
    }

    public void printTransactions() {
        System.out.println("List of transactions for customer: " + this.name);
        for (int i = 0; i < this.transactions.size(); i++) {
            if (i == 0){
                System.out.println("#" + (i+1) + " transaction: " + transactions.get(i) + " - INITIAL TRANSACTION");
                continue;
            }
            System.out.println("#" + (i+1) + " transaction: " + transactions.get(i));
        }
    }


}
