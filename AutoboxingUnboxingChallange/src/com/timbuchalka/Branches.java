package com.timbuchalka;

import java.util.ArrayList;

public class Branches {

    private String branchName;
    private ArrayList<Customer> customersList;

    public Branches(String branchName) {
        this.branchName = branchName;
        this.customersList = new ArrayList<>();
    }

    public String getBranchName() {
        return branchName;
    }

    public ArrayList<Customer> getCustomersList() {
        return customersList;
    }

    /////////////////////////////// ADD CUSTOMER
    public void addCustomer(String name, double initialTransaction) {

        Customer customer = new Customer(name, initialTransaction);
        if (findCustomer(customer.getName()) >= 0) {
            System.out.println("\nCustomer with that name has already been created in " + this.branchName);
        } else {
            customersList.add(customer);
            System.out.println("\nCustomer with name: " + name + " in " + this.branchName + " has been created. " +
                    "Initial amount of money: " + initialTransaction);
        }

    }
    ///////////////////////////////


    /////////////////////////////// NEW TRANSACTION
    public void newTransaction(String name, double transaction) {

        if (findCustomer(name) >= 0) {
            this.customersList.get(findCustomer(name)).newTransaction(transaction);
            System.out.println("\n" + name + "'s transaction for amount of " + transaction + " has been done.");
        } else {
            System.out.println("\nCustomer with that name has not been found. Please, enter correct data");
        }

    }
    ///////////////////////////////


    /////////////////////////////// PRINT ALL CUSTOMERS
    public void printListOfCustomers() {
        System.out.println("\nList of the customers in " + this.branchName+":");
        for (int i = 0; i < this.customersList.size(); i++) {
            System.out.println("#" + (i+1) + " customer name: " + this.customersList.get(i).getName() + ".");
        }
    }
    ///////////////////////////////


    /////////////////////////////// PRINT CUSTOMER'S OPERATIONS
    public void printCustomerTransactions(String name){
        this.customersList.get(findCustomer(name)).printTransactions();
    }
    ///////////////////////////////


    /////////////////////////////// FIND CUSTOMER
    private int findCustomer(Customer contact) {
        return this.customersList.indexOf(contact);
    }

    private int findCustomer(String customerName) {
        for (int i = 0; i < this.customersList.size(); i++) {
            Customer customer = this.customersList.get(i);
            if (customer.getName().equals(customerName)) {
                return i;
            }
        }
        return -1;
    }
    ///////////////////////////////


    /////////////////////////////// QUERY CUSTOMER
    public void queryCustomer(String customerName){
        if(findCustomer(customerName)>=0){
            System.out.println("Customer with that name exists.");
        }else{
            System.out.println("Customer with that name does not exist.");
        }
    }
    ///////////////////////////////


}
