package com.timbuchalka;

public class Printer {

    private double tonerLevel;
    private int numberOfPagesPrinted;
    private boolean isDuplexPrinter;

    public Printer(int tonerLevel, boolean isDuplexPrinter) {
        if(tonerLevel>=0 && tonerLevel<=100) {
            this.tonerLevel = tonerLevel;
        }else{
            this.tonerLevel = -1;
        }
        this.numberOfPagesPrinted = 0;
        this.isDuplexPrinter = isDuplexPrinter;
    }

    public void fillUpToner(int levelToTonerAdded) {

        if (levelToTonerAdded > 0) {

            if (this.tonerLevel+levelToTonerAdded>100){
                System.out.println("Maximum toner level reached.");
            }else{
                this.tonerLevel = this.tonerLevel + levelToTonerAdded;
                System.out.println("Toner level increased about " + levelToTonerAdded + "%. Total level of the toner is "
                        + this.tonerLevel);
            }
        } else {
            System.out.println("Invalid operation. Add some level of the toner.");
        }

    }

    public void printPage(int numberOfPagesToPrint) {

        if (numberOfPagesToPrint <= 0) {
            System.out.println("Invalid operation. Try again.");
        } else {
            this.numberOfPagesPrinted += numberOfPagesToPrint;
            this.tonerLevel = this.tonerLevel - numberOfPagesToPrint*0.1;
            System.out.println("Printing finished.");
        }

    }

    public void getTonerLevel() {
        System.out.println("Toner level is: " + this.tonerLevel + "%");
    }

    public void getNumberOfPagesPrinted() {
        System.out.println("Total number of pages that have already been printed is: " + this.numberOfPagesPrinted);
    }

    public void isDuplexPrinter() {

        if(this.isDuplexPrinter){
            System.out.println("This is a duplex printer.");
        }else{
            System.out.println("This is not a duplex printer.");
        }

    }



}
