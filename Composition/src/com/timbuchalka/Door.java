package com.timbuchalka;

public class Door {

    private int height;
    private int width;
    private String leftOrRight;

    public Door(int height, int width, String leftOrRight) {
        this.height = height;
        this.width = width;
        this.leftOrRight = leftOrRight;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public String getLeftOrRight() {
        return leftOrRight;
    }

    public void openTheDoor(){
        System.out.println("Opening the doors.");
    }



}
