package com.timbuchalka;

public class WindowClass {

    private int numberOfWindows;
    private int width;
    private int height;
    private String leftOrRight;

    public WindowClass(int numberOfWindows, int width, int height, String leftOrRight) {
        this.numberOfWindows = numberOfWindows;
        this.width = width;
        this.height = height;
        this.leftOrRight = leftOrRight;
    }

    public int getNumberOfWindows() {
        return numberOfWindows;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public String getLeftOrRight() {
        return leftOrRight;
    }

    public void openTheWindow(){
        System.out.println("Window has been opened.");
    }



}
