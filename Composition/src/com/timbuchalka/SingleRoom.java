package com.timbuchalka;

import java.awt.*;

public class SingleRoom {

    private int size;
    private int numberOfWalls;
    private Door door;
    private WindowClass window;

    public SingleRoom(int size, int numberOfWalls, Door door, WindowClass window) {
        this.size = size;
        this.numberOfWalls = numberOfWalls;
        this.door = door;
        this.window = window;
    }


    public void openTheDoor(){
        door.openTheDoor();
    }
    public void openTheWindow(){
        window.openTheWindow();
    }

    public Door getDoor() {
        return door;
    }
}
