package com.timbuchalka;

import java.util.ArrayList;
import java.util.LinkedList;

public class Album {

    private String albumName;
    private String artist;
    private ArrayList<Song> albumOfSongs;

    public Album(String albumName, String artist) {
        this.albumName = albumName;
        this.artist = artist;
        this.albumOfSongs = new ArrayList<Song>();
    }

    public String getAlbumName() {
        return albumName;
    }

    public ArrayList<Song> getAlbumOfSongs() {
        return albumOfSongs;
    }


    public boolean addNewSong(String title, double duration) {
        if (findSongInAlbum(title) != null) {
            System.out.println("Song already in Album");
            return false;
        } else {
            this.albumOfSongs.add(new Song(title, duration));
            return true;
        }
    }

    public Song findSongInAlbum(String title) {
        for (Song checkedSong : this.albumOfSongs) {
            if (checkedSong.getTitle().equals(title)) {
                return checkedSong;
            }
        }
        return null;
    }

    public boolean addToPlayList(int trackNumber, LinkedList<Song> playlist) {
        int index = trackNumber - 1;
        if ((index >= 0) && (index <= this.albumOfSongs.size())) {
            playlist.add(this.albumOfSongs.get(index));
            return true;
        }
        System.out.println("This album does not have a track " + trackNumber);
        return false;
    }

    public boolean addToPlayList(String title, LinkedList<Song> playList){
        Song checkedSong = findSongInAlbum(title);
        if(checkedSong != null){
            playList.add(checkedSong);
            return true;
        }
        System.out.println("The song " + title + " is not in this album");
        return false;
    }









    public void printSongsInAlbum() {
        System.out.println("List of songs: \n");
        for (int i = 0; i < this.albumOfSongs.size(); i++) {
            System.out.println((i + 1) + ". " + this.albumOfSongs.get(i).getTitle() + " - " +
                    this.albumOfSongs.get(i).getSongDuration());
        }
    }


}
