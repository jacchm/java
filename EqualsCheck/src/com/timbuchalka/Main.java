package com.timbuchalka;

public class Main {

    public static void main(String[] args) {

        Integer malyInt = new Integer(5);
        int drugiInt = 5;

        System.out.println("Integer");
        if (malyInt == drugiInt) {
            System.out.println("Takie same");
        } else {
            System.out.println("nie takie same");
        }
        if (malyInt.equals(drugiInt)) {
            System.out.println("Takie same 2");
        } else {
            System.out.println("Nie takie same 2");
        }

        System.out.println("String");
        String nowyString = "ala ma kota";
        String drugiString = new String("ala ma kota");

        if (nowyString == drugiString){
            System.out.println("takie same");
        }else{
            System.out.println("nie takie same");
        }
        if (nowyString.equals(drugiString)){
            System.out.println("takie same 2");
        }else{
            System.out.println("nie takie same 2");
        }




    }
}
