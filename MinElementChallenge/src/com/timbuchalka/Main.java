package com.timbuchalka;


import java.util.Scanner;

public class Main {

    private static Scanner scanner = new Scanner(System.in);

    public static int[] readIntegers(int count) {

        int[] array = new int[count];

        for (int i = 0; i < count; i++) {

            System.out.println("Wprowadź liczbę " + (i + 1));
            array[i] = scanner.nextInt();

        }

        return array;
    }

    public static int findMin(int[] array) {

        int min = array[0];
        for (int i = 0; i < array.length - 1; i++) {
            if (array[i + 1] < array[0]) {
                min = array[i + 1];
            }
        }
        return min;
    }


    public static void main(String[] args) {

        int[] array;
        array = readIntegers(5);
        System.out.println("min=" + findMin(array));


    }


}
