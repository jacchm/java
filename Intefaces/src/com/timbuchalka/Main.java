package com.timbuchalka;

public class Main {

    public static void main(String[] args) {

//        ITelephone timsPhone;
        ITelephone timsPhone = new Deskphone(123456);
        timsPhone.powerOn();
        timsPhone.callPhone(123456);
        timsPhone.answer();

        timsPhone = new MobilePhone(23455);
        timsPhone.powerOn();
        timsPhone.callPhone(78542);
        timsPhone.answer();


    }
}
