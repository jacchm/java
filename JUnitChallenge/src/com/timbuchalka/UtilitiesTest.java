package com.timbuchalka;

import jdk.jshell.execution.Util;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class UtilitiesTest {

    private Utilities utilities;

    @Before
    public void setUp() throws Exception {
        utilities = new Utilities();
    }
//
//    @After
//    public void tearDown() throws Exception {
//        fail("Test has not been implemented yet");
//    }

    @Test
    public void everyNthChar() {
        assertArrayEquals(new char[]{'e', 'l'}, utilities.everyNthChar(new char[] {'h', 'e', 'l', 'l', 'o'}, 2));
    }
    @Test
    public void everyNthChar_nGreaterThanInputArrayLength () {
        assertArrayEquals(new char[]{'h', 'e', 'l', 'l', 'o'}, utilities.everyNthChar(new char[] {'h', 'e', 'l', 'l', 'o'}, 10));
    }

    @Test
    public void removePairs() {
        assertEquals("ABCDEF", utilities.removePairs("AABCDDEFF"));
        assertEquals("ABCABDEF", utilities.removePairs("ABCCABDEEF"));
        assertNull("Was not null", utilities.removePairs(null));
        assertEquals("A", utilities.removePairs("A"));
        assertEquals("", utilities.removePairs(""));
    }

    @Test
    public void converter() {
        assertEquals(300, utilities.converter(10, 5));
    }

    @Test(expected = ArithmeticException.class)
    public void converter_divisionByZero_ThrowsArithmeticException(){
        utilities.converter(10,0);
    }

    @Test
    public void nullIfOddLength() {
        assertNull(utilities.nullIfOddLength("odd"));
        assertEquals("even", utilities.nullIfOddLength("even"));
    }

}