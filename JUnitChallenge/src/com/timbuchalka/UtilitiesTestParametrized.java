package com.timbuchalka;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class UtilitiesTestParametrized {

    Utilities utilities;

    String input;
    String expected;

    public UtilitiesTestParametrized(String input, String expected) {
        this.input = input;
        this.expected = expected;
    }

    @Before
    public void setUp() {
        this.utilities = new Utilities();
    }

    @Parameterized.Parameters
    public static Collection<Object> testCondition() {
        return Arrays.asList(new Object[][]{
                {"ABCDEFF", "ABCDEF"},
                {"AB88EFFG", "AB8EFG"},
                {"112233445566", "123456"},
                {"ZYZQQB", "ZYZQB"},
                {"A", "A"}
        });
    }

    @Test
    public void removePairs() {
        assertEquals(expected, utilities.removePairs(input));
    }

}
