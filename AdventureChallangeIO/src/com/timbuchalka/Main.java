package com.timbuchalka;

import java.sql.SQLOutput;
import java.util.*;

public class Main {

    private static Locations locations = new Locations();

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        Map<String, String> vocabulary = new HashMap<String, String>();
        vocabulary.put("QUIT", "Q");
        vocabulary.put("NORTH", "N");
        vocabulary.put("SOUTH", "S");
        vocabulary.put("EAST", "E");
        vocabulary.put("WEST", "W");

        int loc = 1;
        while (true) {
            System.out.println(locations.get(loc).getDescription());
            if (loc == 0) {
                break;
            }

            Map<String, Integer> exits = locations.get(loc).getExits();
            System.out.print("Available exits are: ");

            for (String exit : exits.keySet()) {
                System.out.print(exit + ", ");
            }
            System.out.println();

            String direction = scanner.nextLine().toUpperCase();

            if (direction.length()>1) {
                String[] words = direction.split(" ");

                for (String x : words) {
                    if (vocabulary.containsKey(x)) {
                        direction = vocabulary.get(x);
                        break;
                    }
                }
            }

//            PRIMITIVE IDEA
/*            for (int i = 0; i < words.length; i++) {
                if (words[i].equals("NORTH") || words[i].equals("N")) {
                    direction = "N";
                    break;
                } else if (words[i].equals("SOUTH") ||words[i].equals("S")) {
                    direction = "S";
                    break;
                } else if (words[i].equals("WEST") || words[i].equals("W")) {
                    direction = "W";
                    break;
                } else if (words[i].equals("EAST") || words[i].equals("E")) {
                    direction = "E";
                    break;
                } else if (words[i].equals("QUIT") || words[i].equals("Q")){
                    direction = "Q";
                }
            }*/

            if (exits.containsKey(direction)) {
                loc = exits.get(direction);
            } else {
                System.out.println("You can't go in that direction");
            }
            if (!locations.containsKey(loc)) {
                System.out.println("You cannot go in that direction");
            }

        }


    }
}
